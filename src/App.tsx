import React, { FunctionComponent } from 'react'

export const App: FunctionComponent = ({ children }) => <div>{children}</div>
