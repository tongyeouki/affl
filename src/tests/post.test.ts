import chai from 'chai'
import { InMemoryPostRepository } from '../adapters/secondaries/repository'
import { Post } from '../domain/models'

const expect = chai.expect

describe('Posts handler fetches', () => {
  it('With one post if there is one post in the source', async () => {
    const repository = new InMemoryPostRepository()
    const result = repository.one()
    const expected = new Post(
      'Hello world!',
      'John Doe',
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      'Whatever'
    )
    expect(result).to.eql(expected)
  })
})
