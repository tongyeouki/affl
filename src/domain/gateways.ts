import { Post } from './models'

export interface IPostRepository {
  one(uuid: string): Post | null
}
