**1811** 	

- 22 octobre : naissance à Raiding (Doborjan) dans le comitat de Œdenburg/ Sopron en Hongrie.

**1819** 	

- Printemps : Franz présenté par son père à Karl Czerny qui leur donne un programme de travail.

**1820** 	

- 26 novembre : Franz joue devant le comte Michel Esterhazy six magnats hongrois lui offrent une bourse d'étude

**1822** 	

- Mai : élève à Vienne de Czerny (piano) et Salieri (harmonie)

**1823** 	

- 12 décembre : Cherubini refuse d'inscrire Franz au Conservatoire

**1824** 	

- Janvier : Franz remarqué chez la duchesse de Berry, dans les salons parisiens
- 7 mars : concert au Théâtre des Italiens
- mai-juillet : 1ree tournée en Angleterre grand succès

_Sept variations brillantes sur un thème de Rossini pour p. G. 22_  
_Impromptu brillant sur des thèmes de Rossini et Spontini pour p. G. 23_  

**1825** 	

- 17 octobre : création de Don Sanche, l'unique opéra de Liszt, salle Le Peletier

_Don Sanche opéra G. 174_  
_Compositions disparues : 3 sonates 2 concertos 1 quintette_  

**1826** 	

- Tournée dans les provinces françaises et en Suisse

_Etudes pour le piano en douze Exercices G. 1_  

**1827** 	

- 28 août : mort d'Adam Liszt

_Concerto pour piano en la m. (disparu)_  

**1828** 	

- Juillet : le comte de Saint-Cricq impose une rupture entre Franz et son élève Caroline de Saint-Cricq

**1829** 

- Crise mystique Franz se retire du monde et travaille son piano

_Fantaisie pour piano sur la Tyrolienne de l'opéra La fiancée d'Auber G. 369_  

**1830** 	

- 5 décembre : assiste à la création de la Symphonie fantastique de Berlioz

_Symphonie révolutionnaire (inachevée) G. 718_  

**1831** 	

- Amitié avec Chopin, Hiller, Mendelssohn, Berlioz, vie mondaine intense

**1832** 	

- 20 avril : assiste au concert de Paganini à l'Opéra

_Grande fantaisie de bravoure sur La Clochette de Paganini G. 404_  

**1833** 

- Janvier (ou fin 1832) : rencontre la comtesse d'Agoult chez la marquise Le Vayer

_Grande Symphonie fantastique de Berlioz (transcription) G. 598_  

**1834** 	

- Avril : Joseph d'Ortigue présente Liszt à Lamennais

_Harmonies poétiques et religieuses (avec un profond sentiment d'ennui) pour p. G. 27_  
_Apparitions (3) pour p. G. 28_  

**1835** 	

- Mai : s'enfuit en Suisse avec la comtesse d'Agoult

_Réminiscences de La Juive pour p. G. 394_  
_Fantaisie romantique sur deux mélodies suisses pour p. G. 126_  
_Divertissement sur Niobe pour p. G. 403_  
_De la situation des artistes et de leur condition dans la société dans R. G. M. P._  

**1836**

- Mai : concerts à Lyon et à Paris grands succès en dépit du scandale

_Album d'un voyageur pour p. G. 12_  
_Réminiscences des Huguenots-Grande fantaisie dramatique pour p. G. 396_  
_Réminiscences des Puritains pour p. G. 375_  

**1837** 	

- Janvier-mars : élimination savamment parisienne du rival Thalberg

_24 Grandes études (12 écrites seulement) pour p. G. 2_  
_Symphonies n° 5-7 de Beethoven pour p. (transcriptions) G. 592_  
_Hexameron pour p. (avec Chopin, Czerny, Herz, Pixis, Thalberg) G. 376_  
_M. Thalberg. Grande fantaisie, œuvre 22. Ier et 2e Caprice, œuvres 15 et 29 et Compositions pour piano de Robert Schumann dans R. G. M. P._  

**1838** 	

- Liszt vit en Italie et triomphe à Vienne

_Etudes d'exécution transcendante d'après Paganini pour p. G. 3_  
_Schwanengesang pour p. (d'après Schubert) G. 646_  
_Grand galop chromatique pour p. G. 30_  
_Lettres d'un bachelier es musique dans Revue et Gazette Musicale de Paris_  

**1839** 	

- 23 décembre : Liszt arrive à Pest et pour y donner une série de concerts de bienfaisance

_Andante final de Lucie de Lamermoor 2e acte pour p. (d'ap. Donizetti) G. 383_  
_Lucie de Lamermoor. Marche et Cavatine pour p. (d'après Donizetti) G. 384_  

**1840** 	

- 4 janvier : remise du sabre d'honneur par six magnats en costume national
- 11 janvier concert au Théâtre National au profit d'un fond devant servir à créer un Conservatoire National de Musique.

_Liszt invente le récital chez le prince Galitzine à Rome_  
_Magyar Dallok pour p. G. 117a_  
_Sur Paganini à propos de sa mort dans Revue et Gazette Musicale de Paris_  

**1841**

- Mars : Liszt est à Paris y donne trois concerts dont un au profit du monument Beethoven à Bonn et fixe le prix des 
places à 20 francs. Scandale.

_Grande fantaisie sur Don Juan pour p. G. 402_  
_Réminiscences de Robert le diable. Valse infernale pour p. G. 398_  
_Grand Septuor op. 20 de Beethoven (transcription) pour p. G. 595_  

**1842** 

- janvier : Liszt triomphe à Berlin (17 concerts en 2 mois)

_Valse a capriccio sur deux motifs de Lucia et Parisina pour p. G. 387_  

**1843** 	

- Tournée en Russie et en Allemagne

_Tre Sonetti di Petrarca (version chant et piano) G. 269_  

**1844** 

- Janvier : Liszt prend son premier service de chef d'orchestre à Weimar. Le virtuose romantique prend un emploi, à 
temps partiel, de compositeur de cour.
- Avril : rupture avec la comtesse d'Agoult

_Mélodies et Lieder sur des textes de Victor Hugo, Gœthe, Heine G. 274-279_  

**1845** 	

- 25 mai : Liszt est invité par Lamartine à Saint-Point
- juillet-août : participe au festival d'inauguration du monument Beethoven à Bonn

_Beethoven-Kantate (inédite) G. 250_  

**1846**

- Liszt à Vienne-Tournée dans l'Empire austro-hongrois

_Tarentelle di bravura d’après La muette de Portici d'Auber pour p. G. 370_  

**1847**

- Février-mars : Kiev. Liszt donne trois concerts. Il rencontre la princesse Sayn- Wittgenstein. Elle l'invite à 
Woronince. Il accepte.
- 8 juin-13 juillet : séjour de cinq semaines à Constantinople. Liszt n'ira jamais plus loin.

**1848** 	

- 31 janvier : Liszt arrive à Weimar. Il y restera jusqu'en 1860.
- 26 mars : échappée à Dresde, Liszt se brouille avec Schumann mais scelle véritablement son amitié avec Wagner.

_Réminiscences de Lucrezia Borgia (d'après Donizetti) pour p. G. 386_  
_Trois études de concert pour p. G. 6_  
_Ouverture de Tannhauser pour p. (transcription) G. 664_  

**1849** 

- 16 janvier : représentation à Weimar du Tannhauser de Wagner.

_Grand solo de concert pour p. G. 48_  
_Après une lecture de Dante. Fantasia quasi una sonata. pour p. G. 14b7_  

**1850** 	

- 28 août : Liszt crée Lohengrin de Wagner à l'occasion de l'anniversaire de la naissance de Gœthe.

_Fantaisie et fugue sur le choral "Ad nos salutarem undam" pour orgue d'après un motif du Prophète de Meyerbeer G. 
133_  
_111 pages d'un Sardanapale opéra inachevé G. 726_  

**1851**

- Février : Chopin parait en feuilleton dans la France musicale.

_Scherzo und Marsch pour p. G. 53_  
_Versions définitives des 12 Etudes d'exécution transcendantes G. 7 et des Grandes études de Paganini G. 8_  
_Rhapsodies hongroises n° 1 et n° 2 pour p. G. 118 (version définitive)_  

**1852** 	

- 14-21 novembre : 1ere semaine Berlioz

_Bénédiction et Serment, deux motifs du Benvenuto Cellini de Berlioz G. 603_  
_Soirées de Vienne 9 Valses caprices d'après Schubert G. 412_  
_Fantaisie sur des mélodies populaires hongroises pour piano et orchestre G. 150_  

**1853** 	

- 27 février-4 mars : semaine Wagner

_Sonate en si mineur pour p. G. 54_  
_Festklänge poème symphonique n° 7 pour orchestre G. 157_  
_Rhapsodies hongroises n° 3 à 15 pour p. G. 118 (version définitive)_  

**1854** 

- Février-avril : dirige Orpheus, Les Préludes, Mazeppa, Tasso, Festklänge.
- 24 juin : première mondiale de l'Alfonso und Estrella de Schubert

_Eine Faust-Symphonie in drei Charakter bildern d'après Goethe G. 165_  
_Tasso-Lamento e trionfo poème symphonique n° 2 pour orch. G. 152 (vers. déf.)_  
_Héroïde funèbre poème symphonique n° 3 pour orchestre G. 153 (vers. déf.)_  
_Les Préludes poème symphonique n° 5 pour orchestre G. 155 (ver. déf.)_  
_Mazeppa poème symphonique n° 6 pour orchestre G. 156 (vers. déf.)_  
_Orpheus poème symphonique n° 8 pour orchestre G. 158_  
_Hungaria poème symphonique n° 9 pour orchestre G. 159_  

**1855** 	

- 17-22 février : 2de semaine Berlioz

_Prometheus poème symphonique n° 4 pour orchestre G. 154 (vers. déf.)_  
_Prélude et fugue sur le nom de Bach pour orgue G. 134_  
_Années de Pélerinage. Première année : Suisse pour p. G. 14a_  

**1856** 	

- 31 août : création de la Messe de Gran lors de l'inauguration, en présence de l'Empereur, de la basilique 
primatiale d'Ezstergom

_Messe de Gran S. A. T. B., solistes, chœur et orch. G. 183_  
_Concerto n° 1 pour piano et orch. G. 147 (version définitive)_  
_Dante-Symphonie pour orch. et chœur de femmes G. 166_  

**1857** 	

- 5 septembre : création de la Faust-Symphonie

_Ce qu'on entend sur la montagne p. symph. pour orch. n° 1 G. 151 (vers. def.)_  
_Hunnenslacht poème symphonique n° 10 pour orchestre G. 160_  
_Die Ideale poème symphonique n° 11 pour orchestre G. 161_  

**1858** 	

- 11 avril : Liszt reçu solennellement confrater de l'Ordre des franciscains à Pest

_Hamlet poème symphonique n° 12 pour orchestre G. 162_  
_Années de Pélerinage. Deuxième année : Italie pour p. G. 14b_  

**1859**

- 14 février : Liszt démisionne de ses fonctions de maïtre de chapelle suite à une cabale.
- 10 avril : accède à la noblesse en recevant le titre chevalier de l'Ordre de la couronne de Fer de l'Empire 
autrichien.
- 13 décembre : mort de son fils Daniel.

_Psaume XIII pour tenor solo, chœur solistes et orch. G. 184 (vers. déf.)_  
_Mephisto Valse n° 1 pour p. G. 61_  
_Venezia e Napoli pour p. G. 14c_  

**1860** 

- Mars : publication dans le Berliner Echo du manfeste de Brahms, Joachim, Grimm, Scholz contre la musique de l'avenir
- 14 septembre : crise morale profonde, rédige son testament

_Les Morts pour orch. G. 1701 (1ere des Trois Odes funèbres)_  
_Deux épisodes du Faust de Lenau pour orch. G. 169_  
_Der traurige Mönch mélodrame avec piano sur untexte de Lenau G. 355_  

**1861**

- 6 mai-8 juin : séjour à Paris, vie mondaine intense, revoit la comtesse d'Agoult.
- 22 octobre : cinquantième anniversaire de Liszt.

_Le mariage avec la princesse est reporté à la dernière minute, le 21 au soir, par un envoyé du pape. Il n'aura jamais lieu._  
_Valse de l'opéra Faust (d'après Gounod) pour p. G. 391_  

**1862** 	

Liszt se fixe à Rome. Il y restera jusqu'en 1869.

- 11 septembre : mort de sa fille Blandine

_La légende de Sainte Elisabeth oratorio G. 186_  
_Variations sur le motif de Bach : "Weinen, Klagen, Sorgen, Zagen" pour p. G. 65_  

**1863** 

- 11 juillet : Liszt reçoit la visite du Pape Pie IX qui le prend en grande affection.

_Zwei Konzertetüden pour p. G. 10_  
_Deux légendes pour p. G. 66_  
_Symphonies n° 1-4 et 8 de Beethoven pour p. (transcriptions) G. 592_  
_Pie IX. Hymne pontifical pour orgue G. 137_  

**1864** 	

- 22-26 août : troisième du Tonkunstler-Verein à Karlsruhe où sont joués avec grand succès : le Psaume XIII, 
Méphisto-Valse, Festklänge, Sonate.
- mars : mort du prince Sayn-Wittgenstein. Liszt n'en épouse par pour autant la princesse !

_La Notte pour orch. (deuxième des Trois Odes funèbres) G. 1702_  

**1865** 	

- 25 avril : Liszt reçoit la tonsure des mains du cardinal de Hohenlohe, grand-aumônier du Vatican

_Missa choralis pour chœur mixte et orgue G. 200_  
_Illustrations de L'Africaine (d'après Meyerbeer) pour p. G. 400_  

**1866** 	

- 15 mars : mauvaise exécution de la Messe de Gran à Paris. Liszt est l'objet des quolibets des esprits éclairés de 
la capitale.

_Christus oratorio G. 195_  

**1867**

- 8 juin : création de la Messe du Couronnement en l'Eglise Saint-Matthieu de Buda lors du couronnement de 
François-Joseph comme roi de Hongrie.

_Ungarische Krönungsmesse S.A.T.B., soli, chœur et orch. G. 203_  
_Isoldens Liebestod de Tristan und Isode de Wagner pour p. G. 671_  

**1868** 

- Juillet : Liszt se rend en pélerinage à Spolète, Portiuncula, Assise et Lorette

_Révision d'œuvres de Weber et Schubert_  

**1869**

- 7 janvier : Liszt quitte Rome pour Weimar et commence sa vie trifurquée. Son existence se partagera désormais entre 
trois lieux : Weimar, Budapast et Rome.

**1870**

- Juin : semaine Wagner à l'occasion de la visite du Tsar à Weimar.

**1871**

- 13 juin: Liszt nommé conseiller royal à la Cour de Hongrie, il reçoit par décret spécial, une pension annuelle de 
4000 florins.

**1872**

- 18 mars: Concert à la Redouten-Saal de Vienne en présence de l'Empereur et de toute la Cour.

_Epithalam pour violon et p. G. 360 (1872)_  

**1873** 	

- 29 mai: première exécution de Christus dans son intégralité à Weimar.

_12 große Etüden pour p. (fragment des Technische Stüdien G. 11) (1873)_  

**1876** 	

- 1er août-2 septembre : séjour à Bayreuth pour la création de la Tetralogie.

**1877** 	

- Années de Pélerinage. Troisième année pour p. G. 14d

**1878** 	

- Via crucis, les 14 stations de la Croix pour chœur, soli et orgue G. 224

**1879** 

- Ossa arida pour chœur d'hommes a capella et orgue G. 227

**1881** 	

- Liszt élu membre correspondant de l'Académie des Beaux-Arts de Paris.
- 7 avril : assiste à l'inauguration d'une plaque commémorative sur sa maison natale à Raiding

_Nuages gris pour p. G. 85 (1881)_  
_Mephisto Walzer n° 2 pour orch. G. 173 (1881)_  
_Von der Wiege bis Zum Grabe p. symph. n° 13 pour orch. G. 163 (1881-1882)_  
_Csardas macabre pour p. G. 87 (1881-1882)_  

**1882** 	

_Réminiscences de Simon Boccanegra de Verdi pour p. G. 420_  

**1883** 	

_Mephisto Walzer n° 3 pour p. G. 93_  
_R. W. - Venezia pour p. G. 94_  

**1884** 	

- 23-28 mai : Festival de la Tonkunstler-Versammlung dont Liszt est le président d'honneur. Grand succès de ses œuvres.

_2 Csardas pour p. G. 96 (1884)_  
_Rhapsodies hongroises n° 17-19 pour p. G. 11816-19 (1884-1885)_  

**1885**

- Festival Liszt organisé à Anvers lors de l'Exposition Universelle.

_Bagatelle sans tonalité pour p. G. 98 (1885)_  

**1886** 	

- 16-19 mars : Ouverture à Liège de la série des manifestations européennes organisées à l'occasion des 75 ans du 
compositeur.
- 20 mars-3 avril : Liszt à Paris
- 25 mars : succès triomphal de la Messe de Gran à Saint Eustache.
- 31 juillet : Liszt meurt à Bayreuth.

_Unstern, Sinistre, Disastro pour p. G. 105 (188?)_  

Chronologie simplifiée établie d’après Bruno Moysan, Liszt, Paris, Gisserot, 1999, p. 91-108 (Prix 2000 de l’Association des Professeurs et Maîtres de Conférences de l’Institut d’Etudes Politiques de Paris).

