### Une petite enfance hongroise et viennoise

Franz Liszt naît, à Raiding, aujourd’hui Doborjan, le 22 octobre 1811 dans une région située à l’extrême ouest des
frontières de l’ancien royaume de Hongrie.

C’est cette région qui sera rattachée à l’Autriche, fin 1921, suite au traité de Trianon, sous le nom de Burgenland.

Issu d’une famille germanophone, Liszt est donc fondamentalement un frontalier et ce d’autant plus qu’en 1811 la notion
de frontière et de nation demeure finalement assez floue.

Celui qui deviendra, avec Bartok et Kodaly, le compositeur national de la Hongrie, est avant tout un sujet de l’Empereur
d’Autriche qui vit à Vienne et à une époque qui ne connaît ni la voiture, ni l’avion est peut-être plus encore le fils
d’un employé du fastueux prince Esterhazy, lequel partage son temps entre l’un de ses deux palais à Vienne, son imposant
château d’Eisenstadt et Esterhaza, sa résidence d’été en Hongrie.

C’est donc dans un univers essentiellement rural mais dans la proximité culturelle des Esterhazy que Liszt va développer
ses premiers dons musicaux.

En mai 1822, Liszt devient élève de Czerny et en décembre 1823 arrive à Paris dans le but de parfaire ses études au
Conservatoire.

Le règlement interdisant aux étrangers l’accès au Conservatoire Liszt n’intégrera jamais l’une des prestigieuses classes
de piano de cette institution, ce qui fera de lui fondamentalement un autodidacte.

Liszt n’aura étudié le piano sous la direction d’un professeur, en l’occurrence Czerny, que… dix-huit mois !

### Paris

Les années 1823-1835 sont des années parisiennes.

Malgré des tournées de concert, en Angleterre notamment, en 1825, 1826 et 1827, Liszt vit essentiellement à Paris où il
se partage entre leçons de piano à des jeunes filles de l’aristocratie et de la grande bourgeoisie (ex. Valérie
Boissier), concerts et vie mondaine.

Avec ses jeunes élèves, Liszt, dans ce Paris de la fin de la Restauration et du début de la monarchie de Juillet, lit
Lamartine et Chateaubriand.

Soutenu par la duchesse de Berry dès son arrivée à Paris, Liszt reste jusqu’à son départ pour la Suisse en 1835, suite
au scandale de sa liaison avec la comtesse d’Agoult, le pianiste favori de la société extrêmement aristocratique du
faubourg Saint-Germain.

Cela ne l’empêche pas, après la révolution de 1830, d’être sensible aux idées d’un Lamennais ou d’un Saint-Simon, de
s’affirmer républicain et de s’enthousiasmer pour les productions les plus déroutantes de l’Ecole romantique aussi bien
sur le plan littéraire, Victor Hugo, évidemment, mais aussi Goethe qu’il découvre, dans la traduction de Gérard de
Nerval, grâce à Berlioz, que musical.

C’est en 1833 que Liszt transcrit la Symphonie fantastique de Berlioz et, comme ce dernier, Liszt fait partie de ce
qu’on pourrait appeler un parti allemand qui voit dans les œuvres de Weber et de Beethoven un exemple de modernité.

### Années de pèlerinage

1835 est une rupture majeure.

Liszt quitte Paris pour la Suisse et suivi de peu par la comtesse d’Agoult, enceinte de leur premier enfant.

La raison du voyage est assez prosaïque.

Il s’agit de camoufler autant que possible le scandale d’un enfant adultérin, issu des amours d’une aristocrate très en
vue du faubourg Saint-Germain, qui comptait le roi Charles X parmi les signataires de son contrat de mariage, avec un
pianiste.

Leur génie sera de faire de ce voyage une légende romantique.

Ce couple hors-norme aura trois enfants : Blandine née en 1835, Cosima, née en 1837 et Daniel, né en 1839.

Liszt ne reviendra plus jamais à Paris.

Plus exactement, il n’y habitera plus jamais et ne viendra à Paris que pour de brefs séjours, en général pour y donner
des concerts.

Liszt se retrouve marginalisé dans ce Paris qui l’aura tant nourri artistiquement, culturellement et spirituellement.

Après deux années en Suisse, entrecoupées de séjours à Paris et à Nohant, chez George Sand, le couple part en Italie en
juillet 1837.

Ils y resteront jusqu’en 1839, moment où Liszt entamera cette incroyable tournée de concerts de près de huit ans dans
toute l’Europe, appelée Glanz-Period.

Les années 1835-1839 sont celles d’un couple romantique et voyageur, immensément cultivé et curieux, qui se nourrit
d’impressions de nature, d’œuvres d’art et de littérature.

De ces années de pèlerinage en Suisse et en Italie, il nous reste des écrits très importants de Liszt, un essai
politico-musical, De la situation des artistes et de leur condition dans la société, les fameuses Lettres d’un bachelier
es-musique, et bien sûr des œuvres majeures comme l’Album d’un voyageur qui deviendra ensuite les Années de pèlerinage,
les 12 grandes Etudes qui deviendront les Etudes d’exécution transcendantes.

### Sillonner l’Europe

Les inondations de Pest, en 1839, et pour lesquelles Liszt donne d’importants concerts de charité, sont pour Liszt
l’occasion de redécouvrir sa patrie hongroise et de renouer avec ses années viennoises.

C’est fin 1839 que Liszt commence son immense tournée de concerts dite Glanz-Period et qui reste légendaire dans la
mesure où Liszt aura sillonné toute l’Europe de l’Atlantique à l’Oural, du nord de l’Angleterre au sud de l’Espagne, à
un moment où les transports modernes n’existaient pas encore et où le chemin de fer n’en était qu’à ses débuts.

Cet incroyable périple nomadique finira en 1847.

Après un séjour de cinq semaines à Istanbul à l’invitation du sultan Abdul-Medjid Ier, en juin juillet 1847, Liszt passe
la fin de l’année en Podolie, à Woronince, dans la propriété non plus d’une comtesse mais d’une princesse, la princesse
von Sayn-Wittgenstein qu’il avait rencontrée à Kiev en février de la même année.

Il est impossible de résumer les nombreuses péripéties de cette période haute en couleur où Liszt esquisse aussi de
nombreuses œuvres auxquelles il donnera une forme définitive lorsqu’il sera installé à Weimar à partir de 1848.

Mentionnons toutefois l’épisode du sabre de 1840.

Cet épisode fera rire l’Europe mais à mauvais escient.

Le 4 janvier 1840, six magnats hongrois en costume national remettent à Liszt, après un concert au Théâtre national, un
sabre d’honneur constellé de pierreries.

Dans ce qui aura été une opération de communication politique habilement menée par des élites politiques hongroises
parfaitement conscientes de l’importance de Liszt dans la société européenne de l’époque, il faut voir un épisode majeur
de la construction du nationalisme hongrois au XIXe siècle.

Les vingt années qui suivent seront au contraire nettement plus stables…

### Créer

Lorsque Liszt s’installe à Weimar en 1848, il n’a que 37 ans.

Il n’a rien perdu du dynamisme et des ambitions réformatrices de l’époque où il écrivait De la situation des artistes et
de leur condition dans la société.

Il est, en revanche, profondément las de sa carrière de virtuose.

En lui taillant sur mesure ce poste de « Maître de chapelle en service extraordinaire », le Grand-Duc Charles-Frédéric
de Saxe-Weimar lui fournit la stabilité et le temps qui lui manquaient auparavant et en même temps attire dans sa ville,
qui reste une petite ville malgré son charme et son passé culturel prestigieux, une personnalité de premier plan.

Le résultat ne se fera pas attendre.

En treize ans, de 1848 à 1861, Liszt, non sans difficultés, fait de Weimar une prestigieuse capitale culturelle.

La liste des opéras donnés en première et, dans certains cas, créés (plus de 30 dont Tannhäuser, Lohengrin, Genoveva,
Benvenuto Cellini, Les Huguenots, Ernani et I due Foscari), donne le vertige.

Liszt soutient les jeunes compositeurs et ses collègues romantiques Schumann, Wagner, Mendelssohn, Berlioz pour lequel
il organise deux semaines consacrées à ses œuvres en novembre 1852 et février 1855.

Sur le plan personnel, c’est le moment où Liszt révise et établit les versions définitives des grandes œuvres des années
1830-1848 (Années de pèlerinage à partir de l’Album d’un voyageur, Harmonies poétiques et religieuses, Etudes,
Rhapsodies hongroises à partir des cycles hongrois, Magyar Dallok, Magyar Rapsodiak, des années 1840-1847), compose sa
monumentale Sonate en si mineur, ses grandes œuvres orchestrales (Faust-Symphonie, Dante-Symphonie, douze des Poèmes
symphoniques sur treize), ses œuvres pour piano et orchestre (deux Concertos, Totentanz, Fantaisie hongroise), un
certain nombre de grandes œuvres de musique religieuse (Messe de Gran, Sainte Elisabeth, Psaume XIII), sa grande
Fantaisie et fugue sur le choral « Ad nos ad salutarem undam » pour orgue.

Liszt y développe une esthétique musicale romantique, en rupture avec l’héritage classique, fondée sur le renouvellement
de la musique par la poésie et la liberté de la forme.

C’est en effet à ce moment-là que, partant en partie des expériences formelles des fantaisies sur des thèmes d’opéras
des années 1835-1842, il invente un mode de gestion très original de la forme longue dont la Sonate en si mineur, Après
une lecture de Dante, la Fantaisie et fugue sur le choral « Ad nos ad salutarem undam », le premier mouvement, « Faust
», de la Faust-Symphonie ou encore les deux Concertos sont les exemples les plus accomplis.

Liszt quitte Weimar pour Rome en 1861.

A Rome, il va tenter de donner corps à un projet ancien de renouvellement de la musique religieuse.

Ce projet est chez Liszt une vieille préoccupation, datant des années 1830 et part du constat, développé dans les
milieux catholiques de la France de l’après-Révolution, sous l’impulsion notamment du Génie du Christianisme de
Chateaubriand et de personnalités comme l’abbé de Solesmes, Dom Guéranger, ou Montalembert, du déficit d’intériorité de
la musique religieuse de l’époque, musique qui avait fini par faire pencher du côté de la superficialité mondaine
l’esthétique fastueuse et décorative héritée de la contre-réforme baroque et du style galant classique.

Liszt, soutenu par le catholicisme ardent de la princesse von Sayn-Wittgenstein, va se faire l’apôtre d’une sorte de
néo-gothique musical fondé sur la résurrection du chant grégorien et un retour aux polyphonistes des XVe et XVIe siècle,
en particulier Palestrina.

On doit à ce projet de très grandes œuvres : Christus notamment ou encore la Missa choralis et le Requiem pour voix
d’hommes.

Malgré un certain soutien du Pape Pie IX, un engagement personnel très fort, Liszt ira jusqu’à entrer dans le Tiers
ordre franciscain et recevra même les ordres mineurs, ce qui aura suscité de grandes incompréhensions et le conduira à
essuyer force quolibets, le grand musicien n’arrivera pas à faire de Rome le centre d’une musique religieuse touchée par
la grâce médiévale et solesmienne.

On peut d’ailleurs se demander si Rome était la ville la plus susceptible de devenir la capitale du néo-gothique
musical… ! Il est probable que non mais force est de constater que le combat de Liszt s’achèvera par une victoire
posthume : le fameux Motu proprio de Pie X en 1903 dont le chant du cygne sera, au milieu du XXe siècle, le Requiem de
Duruflé.

### Circuler entre Rome, Weimar et Budapest

Les quinze dernières années de la vie de Liszt sont finalement assez tristes et les œuvres si étranges, si modernistes
et si dépouillées de la fin de sa vie, Via crucis, Csardas macabres etc… sont l’expression d’une sorte d’implosion.
Liszt meurt en juillet 1886 au moment où, pourtant, il entamait un fabuleux retour sur les devants de la scène, sorte de
réparation de si longues années d’incompréhensions et de moqueries.

Si l’on pense aux concerts mémorables donnés à Londres et à Paris en mars avril 1886, on se demande quel aurait pu être
le come-back européen de celui qui, ayant enterré tous les collègues de sa génération, Mendelssohn, Chopin, Schumann,
Berlioz, Wagner, était finalement le seul survivant de la grande épopée romantique de la première moitié du XIXe siècle.

A partir de 1869 et ce jusqu’à sa mort, Liszt redevient nomade.

Pendant dix-sept ans, chaque année, il va partager sa vie entre Weimar, Rome et Budapest.

En circulant entre ces trois villes, dont Paris est la grande absente, comme Vienne d’ailleurs, Liszt va renoncer à
toute forme de stabilité identitaire.

Rome, c’est avant tout la ville des papes et du catholicisme romain, et Liszt, qui n’est pas dupe du mépris que suscite
le catholicisme chez les esprits philosophes et éclairés du XIXe siècle, assume la dimension provocatrice de son
attachement à Rome et au catholicisme.

Si Rome est la quintessence de la latinité catholique, Weimar est une sorte de quintessence de la germanité.

Elle est un des lieux fondateurs du luthéranisme, ville voisine de la Wartburg où Luther a traduit la Bible en allemand.

C’est aussi la ville romantique de Goethe et de Schiller dont Liszt a fait un haut-lieu culturel innovant dans le
contexte d’un Reich en plein développement.

L’axe Weimar-Rome cristallise des antagonismes que Liszt se refuse à synthétiser pleinement.

Quant à Budapest, elle n’est ni Rome, ni Weimar.

Elle n’est ni catholique, ni protestante, ni du Sud, ni du Nord et probablement tout cela à la fois. Avant tout, elle
associe la liberté aux promesse du nouveau car elle est en situation de conquérir la liberté que requiert sa forte
identité, en particulier dans l’usage de sa langue et dans le respect de l’autonomie de sa culture, ensemble de
revendications que Liszt ne pouvait que partager.

Dans la mythologie lisztienne, le tzigane hongrois est d’ailleurs l’incarnation même de la liberté absolue.

Pourtant, à partir du moment où Liszt refuse de s’installer totalement à Budapest et lui dispute Rome et Weimar, cela
veut dire qu’il refuse de s’identifier pleinement au particularisme national hongrois tout simplement parce que
l’enfermement est l’ennemi de la liberté et de l’imagination.

Bruno Moysan