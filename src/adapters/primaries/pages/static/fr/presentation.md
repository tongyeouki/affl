
L’Association française Franz Liszt a vu le jour en mars 1973 à l’initiative de Madame Blandine Ollivier-de Prévaux (1894-1981), arrière-petite-fille de Franz Liszt.

Madame de Prévaux s’est entourée des meilleurs musicologues et interprètes de l’époque, notamment Bernard Gavoty et Serge Gut, musicologues, France Clidat et Georges Cziffra pianistes de renommée internationale ou encore Artur Rubinstein

Le but de l’association était alors : l’étude des œuvres et de l'action de Franz Liszt en France en particulier et le développement des relations avec les associations étrangères dans le monde entier.

Roch Serra, passionné par l’œuvre et la personnalité du grand musicien romantique, et grand ami de Georges Cziffra, prend en main le destin de l'Association à la suite du décès de Blandine Ollivier-de Prévaux en 1981. Il en est le président jusqu’en 2009.

Bruno Moysan est l’actuel président de l’association.