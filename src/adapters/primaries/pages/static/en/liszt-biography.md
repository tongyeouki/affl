### A Hungarian and Viennese Early Childhood

Franz Liszt was born in Raiding, today Doborjan, on October 22, 1811, in a region located at the far western edge of the
former Kingdom of Hungary.

This region would later become part of Austria in late 1921, following the Treaty of Trianon, under the name Burgenland.

Coming from a German-speaking family, Liszt was fundamentally a border dweller, particularly since, in 1811, the
concepts of borders and nationhood were still quite vague.

The man who would become, alongside Bartók and Kodály, the national composer of Hungary, was, above all, a subject of
the Emperor of Austria, residing in Vienna. In an era that knew neither cars nor airplanes, he was perhaps even more so
the son of an employee of the opulent Prince Esterházy, who divided his time between one of his two palaces in Vienna,
his imposing castle in Eisenstadt, and Esterháza, his summer residence in Hungary.

Thus, it was in a predominantly rural environment but within the cultural proximity of the Esterházy family that Liszt
developed his first musical talents.

In May 1822, Liszt became a pupil of Czerny, and in December 1823, he arrived in Paris with the intention of furthering
his studies at the Conservatoire.

Due to a regulation that prohibited foreigners from attending the Conservatoire, Liszt never joined one of the
prestigious piano classes at the institution, which essentially made him a self-taught musician.

Liszt studied piano under the tutelage of Czerny for only... eighteen months!

### Paris

The years 1823-1835 were spent in Paris.

Despite concert tours, notably in England in 1825, 1826, and 1827, Liszt lived primarily in Paris, where he divided his
time between giving piano lessons to young girls from the aristocracy and the upper bourgeoisie (e.g., Valérie Boissier)
, performing concerts, and participating in social life.

With his young students, Liszt, in this Paris at the end of the Restoration and the beginning of the July Monarchy, read
Lamartine and Chateaubriand.

Supported by the Duchess of Berry upon his arrival in Paris, Liszt remained, until his departure for Switzerland in
1835, following the scandal of his affair with the Countess d'Agoult, the favored pianist of the extremely aristocratic
society of the Faubourg Saint-Germain.

Nevertheless, after the 1830 revolution, he became sensitive to the ideas of Lamennais and Saint-Simon, declared himself
a republican, and became enthusiastic about the most radical productions of the Romantic School, both in literature (
Victor Hugo, of course, but also Goethe, whom he discovered in Gérard de Nerval's translation thanks to Berlioz) and in
music.

In 1833, Liszt transcribed Berlioz's *Symphonie fantastique*, and, like Berlioz, Liszt was part of what could be called
a "Germanic party" that viewed the works of Weber and Beethoven as examples of modernity.

### Years of Pilgrimage

1835 marked a major turning point.

Liszt left Paris for Switzerland, soon followed by the pregnant Countess d'Agoult, carrying their first child.

The reason for their journey was rather prosaic.

It was a matter of concealing as much as possible the scandal of an illegitimate child, born of the affair between a
prominent aristocrat from the Faubourg Saint-Germain—whose marriage contract was signed by King Charles X—and a pianist.

Their genius was to turn this journey into a romantic legend.

This unconventional couple would have three children: Blandine, born in 1835, Cosima, born in 1837, and Daniel, born in

1839.

Liszt would never return to Paris again.

More precisely, he would never again live in Paris, visiting only for brief stays, generally for concerts.

Liszt found himself marginalized in Paris, which had so nourished him artistically, culturally, and spiritually.

After two years in Switzerland, interspersed with stays in Paris and at Nohant, with George Sand, the couple left for
Italy in July 1837.

They would remain there until 1839, when Liszt began his incredible concert tour, the *Glanz-Period*, a nearly
eight-year journey across Europe.

The years 1835-1839 were those of a romantic, traveling couple, immensely cultured and curious, nourished by impressions
of nature, works of art, and literature.

From these years of pilgrimage in Switzerland and Italy, we have significant writings by Liszt, such as the
political-musical essay *On the Situation of Artists and Their Condition in Society*, the famous *Letters of a Bachelier
en Musique*, and, of course, major works like *Album d’un Voyageur*, which would later become *Années de Pèlerinage*,
the twelve *Études* that would become the *Transcendental Études*, and the *Hungarian Rhapsodies* of the 1840s-1847.

### Touring Europe

The floods in Pest in 1839, for which Liszt gave important charity concerts, were an opportunity for him to rediscover
his Hungarian homeland and reconnect with his Viennese years.

It was at the end of 1839 that Liszt began his grand concert tour, known as the *Glanz-Period*, which remains legendary
due to the fact that Liszt traveled across all of Europe—from the Atlantic to the Urals, from northern England to
southern Spain—at a time when modern transportation was nonexistent and railroads were still in their infancy.

This incredible nomadic journey would end in 1847.

After a five-week stay in Istanbul at the invitation of Sultan Abdul-Medjid I in June and July 1847, Liszt spent the
rest of the year in Podolia, at Woronince, on the property of a princess, the Princess von Sayn-Wittgenstein, whom he
had met in Kiev earlier that year.

It is impossible to summarize all the colorful events of this period, during which Liszt also sketched many works that
he would later finalize when he settled in Weimar in 1848.

One notable episode from this period is the *sabre incident* of 1840.

This episode, which amused Europe but for the wrong reasons, occurred on January 4, 1840, when six Hungarian magnates in
national costume presented Liszt with a jeweled sabre after a concert at the National Theatre.

What had been a cleverly orchestrated political communication move by Hungarian political elites, fully aware of Liszt's
importance in European society at the time, should be seen as a major episode in the construction of Hungarian
nationalism in the 19th century.

The next twenty years would, by contrast, be much more stable...

### Creating

When Liszt settled in Weimar in 1848, he was only 37 years old.

He had lost none of the dynamism and reforming ambitions of the period when he wrote *On the Situation of Artists and
Their Condition in Society*.

However, he was deeply weary of his career as a virtuoso.

By granting him the position of "Maestro di Cappella in Extraordinary Service," Grand Duke Charles-Frederick of
Saxe-Weimar provided him with the stability and time he had previously lacked, while simultaneously attracting a leading
figure to his small city, which, despite its charm and prestigious cultural past, was still a modest town.

The results were immediate.

In thirteen years, from 1848 to 1861, Liszt, not without difficulties, transformed Weimar into a prestigious cultural
capital.

The list of operas premiered there (over 30, including *Tannhäuser*, *Lohengrin*, *Genoveva*, *Benvenuto Cellini*, *Les
Huguenots*, *Ernani*, and *I due Foscari*) is staggering.

Liszt supported young composers and his Romantic colleagues, including Schumann, Wagner, Mendelssohn, and Berlioz, for
whom he organized two weeks dedicated to his works in November 1852 and February 1855.

On a personal level, this was also the time when Liszt revised and established the final versions of major works from
the 1830s-1848 period (*Années de Pèlerinage* from *Album d’un Voyageur*, *Harmonies poétiques et religieuses*, *Études*
, *Hungarian Rhapsodies* from the Hungarian cycles, *Magyar Dallok*, *Magyar Rapsodiak*, from the 1840s-1847), composed
his monumental *Sonata in B Minor*, his major orchestral works (*Faust Symphony*, *Dante Symphony*, twelve of the *
Symphonic Poems*), his piano and orchestra works (two *Concertos*, *Totentanz*, *Hungarian Fantasy*), several large
religious works (*Gran Mass*, *St. Elizabeth*, *Psalm XIII*), and his great *Fantasy and Fugue on the Choral "Ad nos ad
salutarem undam"* for organ.

Liszt developed a Romantic musical aesthetic, breaking with classical traditions, based on the renewal of music through
poetry and freedom of form.

It was during this period that he, partly drawing from the formal experiences of opera fantasias from the years
1835-1842, invented a highly original way of handling long forms, of which the *Sonata in B Minor*, *After a Reading of
Dante*, the *Fantasy and Fugue on the Choral "Ad nos ad salutarem undam"*, the first movement, "Faust," of the *Faust
Symphony*, and the two *Concertos* are the most accomplished examples.**

Liszt left Weimar for Rome in 1861.

In Rome, he would attempt to realize an old project of renewing sacred music. This project had long preoccupied Liszt,
dating back to the 1830s, and was rooted in the observation, particularly in post-Revolutionary France, within Catholic
circles (notably inspired by Chateaubriand’s *Génie du Christianisme* and figures like Abbé de Solesmes, Dom Guéranger,
and Montalembert), of the lack of spirituality in the religious music of the time. This music had drifted towards
superficiality, influenced by the ostentatious and decorative aesthetics of the Baroque Counter-Reformation and the
Galant style.

Supported by the devout Catholicism of Princess von Sayn-Wittgenstein, Liszt became an advocate for a kind of musical
Neo-Gothicism, centered on the resurrection of Gregorian chant and a return to the polyphony of the 15th and 16th
centuries, particularly that of Palestrina.

This project yielded some of his greatest works: *Christus*, the *Missa choralis*, and the *Requiem* for male voices.

Despite a certain degree of support from Pope Pius IX, and Liszt’s personal commitment, even entering the Third Order of
St. Francis and receiving minor orders, which caused considerable misunderstanding and led to much ridicule, the great
composer could not succeed in making Rome the center of a sacred music movement touched by medieval and Solesmes grace.

One could even question whether Rome was truly the city best suited to become the capital of musical Neo-Gothicism. It
is likely not. However, it is undeniable that Liszt’s struggle would end in posthumous victory: the famous *Motu
Proprio* of Pius X in 1903, whose swan song would be, in the mid-20th century, the *Requiem* of Maurice Duruflé.

### Circulating between Rome, Weimar, and Budapest

The last fifteen years of Liszt’s life were, in the end, quite sad. The strange, modernist, and sparse works from this
late period, such as *Via Crucis* and *Csárdás Macabres*, reflect a sort of internal implosion. Liszt died in July 1886,
at a time when he was staging a remarkable comeback, a sort of redemption after so many years of misunderstandings and
mockery.

If we think of the memorable concerts given in London and Paris in March and April 1886, one might wonder what his
European comeback could have been like. Having outlived all his colleagues from his generation—Mendelssohn, Chopin,
Schumann, Berlioz, and Wagner—Liszt was ultimately the sole survivor of the great Romantic epic of the first half of the
19th century.

From 1869 until his death, Liszt became a wanderer once again.

For seventeen years, each year he would divide his time between Weimar, Rome, and Budapest.

By circulating between these three cities—Paris being the notable absence, as is Vienna—Liszt renounced any form of
stable identity.

Rome was, above all, the city of the papacy and Roman Catholicism, and Liszt, who was not deceived by the disdain
Catholicism provoked among the philosophical and enlightened minds of the 19th century, embraced the provocative nature
of his attachment to Rome and Catholicism.

If Rome represented the quintessence of Catholic Latinity, Weimar represented the quintessence of Germanness.

Weimar was one of the founding places of Lutheranism, located near the Wartburg, where Luther translated the Bible into
German. It was also the Romantic city of Goethe and Schiller, whose cultural legacy Liszt transformed into an innovative
cultural center in the context of a rapidly developing Reich.

The axis between Weimar and Rome crystallized antagonisms that Liszt refused to fully reconcile.

As for Budapest, it was neither Rome nor Weimar.

It was neither Catholic nor Protestant, neither Southern nor Northern, and perhaps all of these at once. Above all,
Budapest associated freedom with the promises of a new future, as it was in the process of acquiring the freedom
demanded by its strong identity—particularly in the use of its language and respect for its cultural autonomy—values
that Liszt could not help but share.

In Liszt’s mythology, the Hungarian gypsy was the very incarnation of absolute freedom.

However, from the moment Liszt refused to fully settle in Budapest, competing with Rome and Weimar, this signified his
refusal to fully identify with Hungarian national particularism, simply because confinement was the enemy of freedom and
imagination.

Bruno Moysan