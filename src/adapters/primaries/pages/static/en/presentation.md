The aim of the Association française Franz Liszt is to study and promote the thought and action of Franz Liszt and his contemporaries.

By participating in the organization of cultural events around the work of Franz Liszt,
By encouraging, encouraging and producing any other research, study, creation or dissemination activity relating to the work of Franz Liszt and his contemporaries.
