# AFFL

## Présentation

Projet de page statique pour le site de l'Association française Franz Liszt. 

## Usage

La version de NodeJs est la `v14.16.1`. 

```
$ npm install
$ npm start
```

Dans un fichier `.env` : 

```
## REACT_APP_ENV= dev || prod
REACT_APP_ENV=dev 
REACT_APP_ASSOCIATION_EMAIL=contact@example.com
```

## Réutilisation

Ce code et les contenus sont à usage seul de l'Association Française Franz 
Liszt. 